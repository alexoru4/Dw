import django_tables2 as tables


class KeyIndicatorsTable(tables.Table):
    indicator = tables.Column()
    indicator.verbose_name = 'Показник'
    date_to = tables.Column()
    date_to.verbose_name = '2015-11-18'
    date_from = tables.Column()
    date_from.verbose_name = '2015-11-17'
    diff_by_percents = tables.Column()
    diff_by_percents.verbose_name = 'Різниця в %'
    diff_by_values = tables.Column()
    diff_by_values.verbose_name = 'Різниця'

    class Meta:
        template_name = 'django_tables2/bootstrap.html'


class ProdSellsTable(tables.Table):
    prod_name = tables.Column()
    prod_name.verbose_name = 'Назва товару'
    diff_by_qty = tables.Column()
    diff_by_qty.verbose_name = 'Зміна к-сті продаж'
    diff_by_turnover = tables.Column()
    diff_by_turnover.verbose_name = 'Зміна обороту'

    class Meta:
        template_name = 'django_tables2/bootstrap.html'

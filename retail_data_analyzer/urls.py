from django.urls import path
from .views import key_indicators, prod_sells
from django.views.decorators.cache import cache_page

urlpatterns = [
    path('key-indicators',  cache_page(60 * 15)(key_indicators), name='key_indicators'),
    path('prod-sells/<slug:slug>/',  cache_page(60 * 15)(prod_sells), name='prod_sells'),
]
from django.apps import AppConfig


class RetailDataAnalyzerConfig(AppConfig):
    name = 'retail_data_analyzer'
